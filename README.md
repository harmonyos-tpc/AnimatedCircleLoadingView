# Animated Circle Loading View

A determiante/indetermiante loading view animation.

<img src="https://gitee.com/openharmony-tpc/AnimatedCircleLoadingView/raw/master/gifs/AnimatedCircleLoadingView_failure.gif" width="50%" height="50%"/>
<img src="https://gitee.com/openharmony-tpc/AnimatedCircleLoadingView/raw/master/gifs/AnimatedCircleLoadingView_ok.gif" width="50%" height="50%"/>

## Gradle
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:AnimatedCircleLoadingView:1.0.2'
```

## Usage
----
Add AnimatedCircleLoadingView to your layout and define mainColor and secondaryColor as custom attributes:

```java
<com.github.jlmd.animatedcircleloadingview.AnimatedCircleLoadingView
        ohos:id="$+id:circle_loading_view"
        ohos:width="250vp"
        ohos:height="250vp"
        ohos:background_element="$color:background"
        ohos:center_in_parent="true"
        ohos:mainColor="$color:main_color"
        ohos:secondaryColor="$color:secondary_color"
        ohos:textColor="white"
        />
```

## Determinate
Start determinate:

```java
animatedCircleLoadingView.startAnimated();
```

Modify percent:
```java
animatedCircleLoadingView.setPercent(10);
```

If percent is 100, the animation ends with success animation.
On error you must call stopFailure() method, then the application ends with failure animation.

##### Indeterminate
Start indeterminate:

```java
animatedCircleLoadingView.startIndeterminate();
```

Stop with success:

```java
animatedCircleLoadingView.stopOk();
```

Stop with failure:

```java
animatedCircleLoadingView.stopFailure();
```

Reset loading:

```java
animatedCircleLoadingView.resetLoading();
```

License
----
```
Copyright 2015 José Luis Martín

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```