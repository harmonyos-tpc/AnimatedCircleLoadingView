package com.github.jlmd.animatedcircleloadingview.component;

import com.github.jlmd.animatedcircleloadingview.animator.MyValueAnimator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * @author jlmd
 */
public class PercentIndicatorView extends Text {
    private final int parentWidth;
    private Color textColor = Color.WHITE;

    public PercentIndicatorView(Context context, int parentWidth, Color textColor) {
        super(context);
        this.parentWidth = parentWidth;
        this.textColor = textColor;
        init();
    }

    private void init() {
        int textSize = (35 * parentWidth) / 700;
        setTextSize(textSize, TextSizeType.FP);
        setTextColor(this.textColor);
        setTextAlignment(TextAlignment.CENTER);
        setAlpha(0.8f);
    }

    public void setPercent(int percent) {
        setText(percent + "%");
    }

    public void startAlphaAnimation() {
        MyValueAnimator alphaAnimation = new MyValueAnimator();
        alphaAnimation.setFloat(1.0f, 0.0f);
        alphaAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setAlpha(value);
            }
        });
        alphaAnimation.setDuration(700);
        alphaAnimation.start();
    }
}
