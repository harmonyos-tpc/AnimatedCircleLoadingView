package com.github.jlmd.animatedcircleloadingview.component;

import com.github.jlmd.animatedcircleloadingview.animator.AnimationState;
import com.github.jlmd.animatedcircleloadingview.animator.MyStateChange;
import com.github.jlmd.animatedcircleloadingview.animator.MyValueAnimator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * @author jlmd
 */
public class InitialCenterCircleView extends ComponentViewAnimation implements Component.DrawTask {
    private Paint paint;
    private RectFloat oval;
    private float minRadius;
    private float currentCircleWidth;
    private float currentCircleHeight;

    public InitialCenterCircleView(Context context, int parentWidth, Color mainColor,
                                   Color secondaryColor) {
        super(context, parentWidth, mainColor, secondaryColor);
        init();
        addDrawTask(this);
    }

    private void init() {
        initOval();
        initPaint();
    }

    private void initPaint() {
        paint = new Paint();
        paint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        paint.setColor(mainColor);
        paint.setAntiAlias(true);
    }

    private void initOval() {
        oval = new RectFloat();
        minRadius = (15 * parentWidth) / 700;
        currentCircleWidth = minRadius;
        currentCircleHeight = minRadius;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        drawCircle(canvas);
    }

    public void drawCircle(Canvas canvas) {
        RectFloat oval = new RectFloat(parentCenter - currentCircleWidth, parentCenter - currentCircleHeight,
                parentCenter + currentCircleWidth, parentCenter + currentCircleHeight);
        canvas.drawOval(oval, paint);
    }

    public void startTranslateTopAnimation() {
        float translationYTo = -(255 * parentWidth) / 700;
        MyValueAnimator translationY = new MyValueAnimator();
        translationY.setFloat(0, translationYTo);
        translationY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setTranslationY(value);
            }
        });
        translationY.setDuration(1100);
        translationY.setStateChangedListener(new MyStateChange() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                setState(AnimationState.MAIN_CIRCLE_TRANSLATED_TOP);
            }
        });
        translationY.start();
    }

    public void startScaleAnimation() {
        MyValueAnimator valueAnimator = new MyValueAnimator();
        valueAnimator.setFloat(minRadius, circleRadius);
        valueAnimator.setDuration(1400);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                currentCircleWidth = value;
                currentCircleHeight = value;
                invalidate();
            }
        });
        valueAnimator.start();
    }

    public void startTranslateBottomAnimation() {
        float translationYFrom = -(260 * parentWidth) / 700;
        float translationYTo = (360 * parentWidth) / 700;
        MyValueAnimator translationY = new MyValueAnimator();
        translationY.setFloat(translationYFrom, translationYTo);
        translationY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setTranslationY(value);
            }
        });
        translationY.setDuration(650);
        translationY.start();
    }

    public void startScaleDisappear() {
        float maxScaleSize = (250 * parentWidth) / 700;
        MyValueAnimator valueScaleWidthAnimator = new MyValueAnimator();
        valueScaleWidthAnimator.setFloat(circleRadius, maxScaleSize);
        valueScaleWidthAnimator.setDuration(260);
        valueScaleWidthAnimator.setDelay(430);
        valueScaleWidthAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                currentCircleWidth = value;
                invalidate();
            }
        });
        valueScaleWidthAnimator.setStateChangedListener(new MyStateChange() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                setState(AnimationState.MAIN_CIRCLE_SCALED_DISAPPEAR);
                currentCircleWidth = circleRadius + (strokeWidth / 2);
                currentCircleHeight = circleRadius + (strokeWidth / 2);
            }
        });
        valueScaleWidthAnimator.start();
        MyValueAnimator valueScaleHeightAnimator = new MyValueAnimator();
        valueScaleHeightAnimator.setFloat(circleRadius, circleRadius / 2);
        valueScaleHeightAnimator.setDuration(260);
        valueScaleHeightAnimator.setDelay(430);
        valueScaleHeightAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                currentCircleHeight = value;
                invalidate();
            }
        });
        valueScaleHeightAnimator.start();
    }

    public void startTranslateCenterAnimation() {
        float translationYFrom = -(260 * parentWidth) / 700;
        MyValueAnimator translationY = new MyValueAnimator();
        translationY.setFloat(translationYFrom, 0);
        translationY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setTranslationY(value);
            }
        });
        translationY.setDuration(600);
        translationY.setStateChangedListener(new MyStateChange() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                setState(AnimationState.MAIN_CIRCLE_TRANSLATED_CENTER);
            }
        });
        translationY.start();
    }

    public void reset() {
        float translationYTo = -(255 * parentWidth) / 700;
        MyValueAnimator translationY = new MyValueAnimator();
        translationY.setFloat(0, translationYTo);
        translationY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setTranslationY(value);
            }
        });
        translationY.setDuration(10);
        translationY.start();
    }

}
