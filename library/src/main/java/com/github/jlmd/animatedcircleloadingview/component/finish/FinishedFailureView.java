package com.github.jlmd.animatedcircleloadingview.component.finish;

import com.github.jlmd.animatedcircleloadingview.ResourceTable;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * @author jlmd
 */
public class FinishedFailureView extends FinishedView {
    public FinishedFailureView(Context context, int parentWidth, Color mainColor, Color secondaryColor,
                               Color tintColor) {
        super(context, parentWidth, mainColor, secondaryColor, tintColor);
    }

    @Override
    protected int getDrawable() {
        return ResourceTable.Media_ic_failure_mark;
    }

    @Override
    protected Color getDrawableTintColor() {
        return tintColor;
    }

    @Override
    protected Color getCircleColor() {
        return secondaryColor;
    }
}