package com.github.jlmd.animatedcircleloadingview.component;

import com.github.jlmd.animatedcircleloadingview.animator.AnimationState;
import com.github.jlmd.animatedcircleloadingview.animator.MyStateChange;
import com.github.jlmd.animatedcircleloadingview.animator.MyValueAnimator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * @author jlmd
 */
public class TopCircleBorderView extends ComponentViewAnimation implements Component.DrawTask {
    private static final int MIN_ANGLE = 25;
    private static final int MAX_ANGLE = 180;
    private Paint paint;
    private RectFloat oval;
    private int arcAngle;
    private boolean isReset = false;

    public TopCircleBorderView(Context context, int parentWidth, Color mainColor, Color secondaryColor) {
        super(context, parentWidth, mainColor, secondaryColor);
        init();
        addDrawTask(this);
    }

    private void init() {
        initPaint();
        initOval();
        arcAngle = MIN_ANGLE;
    }

    private void initPaint() {
        paint = new Paint();
        paint.setColor(mainColor);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setAntiAlias(true);
    }

    private void initOval() {
        float padding = paint.getStrokeWidth() / 2;
        oval = new RectFloat(parentCenter - circleRadius, padding, parentCenter + circleRadius, circleRadius * 2);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (!isReset)
            drawArcs(canvas);
    }

    private void drawArcs(Canvas canvas) {
        canvas.drawArc(oval, new Arc(270, arcAngle, false), paint);
        canvas.drawArc(oval, new Arc(270, -arcAngle, false), paint);
    }

    public void startDrawCircleAnimation() {
        MyValueAnimator valueAnimator = new MyValueAnimator();
        valueAnimator.setFloat(MIN_ANGLE, MAX_ANGLE);
        valueAnimator.setCurveType(Animator.CurveType.DECELERATE);
        valueAnimator.setDuration(400);
        valueAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                arcAngle = (int) value;
                isReset = false;
                invalidate();
            }
        });
        valueAnimator.setStateChangedListener(new MyStateChange() {
            @Override
            public void onStart(Animator animator) {
                isReset = false;
            }

            @Override
            public void onEnd(Animator animator) {
                setState(AnimationState.MAIN_CIRCLE_DRAWN_TOP);
            }
        });
        valueAnimator.start();
    }

    public void reset() {
        arcAngle = 0;
        initOval();
        isReset = true;
        invalidate();
    }
}
