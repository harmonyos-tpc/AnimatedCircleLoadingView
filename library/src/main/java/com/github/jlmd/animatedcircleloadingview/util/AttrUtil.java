/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jlmd.animatedcircleloadingview.util;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;

/**
 * 属性字段工具类
 */
public class AttrUtil {
    private final String TAG = "TAG";
    private AttrSet attrSet;

    public AttrUtil(AttrSet attrSet) {
        this.attrSet = attrSet;
    }

    /**
     * 获取自定义属性颜色值
     *
     * @param attrName     属性字段
     * @param defaultColor 默认颜色值
     * @return 颜色值
     */
    public Color getAttrColor(String attrName, Color defaultColor) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getColorValue();
        }
        return defaultColor;
    }

    /**
     * 获取自定义属性字符串值
     *
     * @param attrName   属性字段
     * @param defaultStr 默认字符串值
     * @return 字符串
     */
    public String getAttrString(String attrName, String defaultStr) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getStringValue();
        }
        return defaultStr;
    }

    /**
     * 获取自定义属性Element值
     *
     * @param attrName       属性字段
     * @param defaultElement 默认Element值
     * @return Element
     */
    public Element getAttrElement(String attrName, Element defaultElement) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getElement();
        }
        return defaultElement;
    }

    /**
     * 获取自定义属性Int值
     *
     * @param attrName   属性字段
     * @param defaultInt 默认Int值
     * @return Int
     */
    public int getAttrInt(String attrName, int defaultInt) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getIntegerValue();
        }
        return defaultInt;
    }

    /**
     * 获取自定义属性Dimen值
     *
     * @param attrName     属性字段
     * @param defaultdimen 默认Dimen值
     * @return Dimen
     */
    public int getAttrDimen(String attrName, int defaultdimen) {
        if (isPresent(attrName)) {
            String value = attrSet.getAttr(attrName).get().toString();
            return attrSet.getAttr(attrName).get().getDimensionValue();
        }
        return defaultdimen;
    }

    /**
     * 获取自定义属性long值
     *
     * @param attrName    属性字段
     * @param defaultLong 默认long值
     * @return long
     */
    public long getAttrLong(String attrName, long defaultLong) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getLongValue();
        }
        return defaultLong;
    }

    /**
     * 获取自定义属性float值
     *
     * @param attrName     属性字段
     * @param defaultFloat 默认float值
     * @return float
     */
    public float getAttrFloat(String attrName, float defaultFloat) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getFloatValue();
        }
        return defaultFloat;
    }

    /**
     * 获取自定义属性boolean值
     *
     * @param attrName       属性字段
     * @param defaultBoolean 默认boolean值
     * @return boolean
     */
    public boolean getAttrBoolean(String attrName, boolean defaultBoolean) {
        if (isPresent(attrName)) {
            return attrSet.getAttr(attrName).get().getBoolValue();
        }
        return defaultBoolean;
    }

    /**
     * 获取非基本类型自定义属性值
     *
     * @param attrName   属性字段
     * @param defaultInt 资源Id值
     * @return 资源Id
     */
    public int getAttrOther(String attrName, int defaultInt) {
        String value;
        if (isPresent(attrName)) {
            value = attrSet.getAttr(attrName).get().getStringValue();
            if (value.contains(":")) {
                return Integer.parseInt(value.split(":")[1]);
            }
        }
        return defaultInt;
    }

    /**
     * 判断属性字段是否存在
     *
     * @param attrName 属性字段
     * @return boolean
     */
    private boolean isPresent(String attrName) {
        return attrSet.getAttr(attrName).isPresent();
    }
}
