package com.github.jlmd.animatedcircleloadingview.component;

import com.github.jlmd.animatedcircleloadingview.animator.AnimationState;
import com.github.jlmd.animatedcircleloadingview.animator.MyStateChange;
import com.github.jlmd.animatedcircleloadingview.animator.MyValueAnimator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * @author jlmd
 */
public class RightCircleView extends ComponentViewAnimation implements Component.DrawTask {

    private int rightMargin;
    private int bottomMargin;
    private Paint paint;

    public RightCircleView(Context context, int parentWidth, Color mainColor, Color secondaryColor) {
        super(context, parentWidth, mainColor, secondaryColor);
        init();
        addDrawTask(this);
    }

    private void init() {
        rightMargin = (150 * parentWidth / 700);
        bottomMargin = (50 * parentWidth / 700);
        initPaint();
    }

    private void initPaint() {
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(secondaryColor);
        paint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        drawCircle(canvas);
    }

    public void drawCircle(Canvas canvas) {
        canvas.drawCircle(getWidth() - rightMargin, parentCenter - bottomMargin, circleRadius, paint);
    }

    public void startSecondaryCircleAnimation() {
        int bottomMovementAddition = (260 * parentWidth) / 700;
        int[] locationOnScreen = getLocationOnScreen();

        MyValueAnimator translateAnimation = new MyValueAnimator();
        translateAnimation.setFloat(locationOnScreen[1], locationOnScreen[1] + bottomMovementAddition);
        translateAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setTranslationY(value);
                setTranslationX(locationOnScreen[0]);
            }
        });
        translateAnimation.setDelay(200);
        translateAnimation.setDuration(1000);
        MyValueAnimator alphaAnimation = new MyValueAnimator();
        alphaAnimation.setFloat(1.0f, 0.0f);
        alphaAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                setAlpha(value);
            }
        });
        alphaAnimation.setDuration(200);
        AnimatorGroup animationSet = new AnimatorGroup();
        animationSet.runSerially(translateAnimation, alphaAnimation);
        animationSet.setStateChangedListener(new MyStateChange() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                setState(AnimationState.SECONDARY_CIRCLE_FINISHED);
            }
        });
        animationSet.start();
    }
}