package com.github.jlmd.animatedcircleloadingview.component;

import com.github.jlmd.animatedcircleloadingview.animator.AnimationState;
import com.github.jlmd.animatedcircleloadingview.exception.NullStateListenerException;

import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * @author jlmd
 */
public abstract class ComponentViewAnimation extends Component {
    protected final int parentWidth;
    protected final Color mainColor;
    protected final Color secondaryColor;

    protected float parentCenter;
    protected float circleRadius;
    protected int strokeWidth;
    private StateListener stateListener;

    public ComponentViewAnimation(
            Context context, int parentWidth, Color mainColor,
            Color secondaryColor) {
        super(context);
        this.parentWidth = parentWidth;
        this.mainColor = mainColor;
        this.secondaryColor = secondaryColor;
        init();
    }

    private void init() {
        hideView();
        circleRadius = parentWidth / 10;
        parentCenter = parentWidth / 2;
        strokeWidth = (12 * parentWidth) / 700;
    }

    public void hideView() {
        setVisibility(Component.HIDE);
    }

    public void setState(AnimationState state) {
        if (stateListener != null) {
            stateListener.onStateChanged(state);
        } else {
            throw new NullStateListenerException();
        }
    }

    public void setStateListener(StateListener stateListener) {
        this.stateListener = stateListener;
    }

    public interface StateListener {

        void onStateChanged(AnimationState state);
    }
}
